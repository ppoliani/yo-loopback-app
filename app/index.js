'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var yosay = require('yosay');
var fs = require('fs');

var YoWebUiGenerator = yeoman.generators.Base.extend({
	promptUser: function() {
        var done = this.async();
 
        // have Yeoman greet the user
        console.log(this.yeoman);
 
        var prompts = [{
            name: 'appName',
            message: 'What is your app\'s name ?'
        }];
 
        this.prompt(prompts, function (props) {
            this.appName = props.appName;
   
            done();
        }.bind(this));
    },

    scaffoldFolders: function(){
    	this.mkdir(".strong-pm");
    	this.mkdir("client");
    	this.mkdir("common");
    	this.mkdir("gulp");
    	this.mkdir("server");

    	this.mkdir("client/app");
        this.mkdir("client/app/components");
        this.mkdir("client/app/components/viewInit");
        this.mkdir("client/app/config");
        this.mkdir("client/app/core");
        this.mkdir("client/app/home");
        this.mkdir("client/app/partial");

        this.mkdir("client/client/assets");
        this.mkdir("client/client/assets/css");
        this.mkdir("client/client/assets/images");

    	this.mkdir("common/models");

        this.mkdir("gulp/build_tasks");

        this.mkdir("server/boot");
        this.mkdir("server/config");
        this.mkdir("server/views");
    },

    copyFiles: function() {
	    this.directory(".strong-pm/", ".strong-pm/");
        this.directory("client/", "client/");
        this.directory("common/", "common");
        this.directory("gulp/", "gulp");
        this.directory("server/", "server/");

        this.copy(".editorconfig", ".editorconfig");
        this.copy(".gitignore", ".gitignore");
        this.copy(".jshintignore", ".jshintignore");
        this.copy(".jshintrc", ".jshintrc");
        this.copy(".npmignore", ".npmignore");

        this.template("client/_bower.json", "client/bower.json", { app_name: this.appName })
            .on("end", function(){ 
                fs.unlink('client/_bower.json', function (err) { if (err) throw err; }); 
            });

	    this.template("_package.json", "package.json", { app_name: this.appName });

        this.template("gulp/_package.json", "gulp/package.json", { app_name: this.appName })
            .on("end", function(){ 
                fs.unlink('gulp/_package.json', function (err) { if (err) throw err; }); 
            });
	},

    installDependencies: function(){
        console.log("\nInstalling Node Dependencies\n");
        
        //this.bowerInstall();

        /*this.npmInstall("", function(){
            console.log("\nInstalling Bower Packages\n");

            

            this.bowerInstall("client/");
        }.bind(this));*/
    }
});

module.exports = YoWebUiGenerator;
