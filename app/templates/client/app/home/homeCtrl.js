/**
 * Home controller
 */
 (function(){
    'use strict';

    function homeCtrl(){
        // region Inner Fields


        // endregion

        // region Inner Methods

        // endregion

        // region Viewmodel

        // endregion

        // region Setup


        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'homeCtrl',
        ctrl: [homeCtrl]
    };

    // endregion

 })();
