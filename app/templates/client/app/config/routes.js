/**
 * App entry point
 */
var httpProviderConfig = (function(){
    'use strict';

    // region Consts

    // This is relative to the nodeJS static files directory !!!
    var BASE_DIR = '/app/';

    // endregion

    // region Inner Methods

    /**
     * Runs the configurations
     */
    function configure(app){
        app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
                $routeProvider
                    .when('/', {
                        templateUrl: _getPath('home/index'),
                        controller: 'homeCtrl as  vm'
                    });

                $routeProvider.otherwise('/');
                $locationProvider.html5Mode(true);
            }]);
    }

    /**
     * Return the paths relative to base dir
     *
     * @param filePath
     * @returns {string}
     * @private
     */
    function _getPath(filePath){
        return BASE_DIR + filePath + '.html';
    }

    // endregion

    // region Public API

    return {
        configure: configure
    };

    // endregion
})();

// region Exports

module.exports = httpProviderConfig;

// endregion