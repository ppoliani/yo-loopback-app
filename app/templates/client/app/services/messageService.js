/**
 * Message service
 */
(function(){
    'use strict';

    function messageService(toastr){

        // region Inner Methods

        function info(title, msg){
            toastr.info(title, msg);
        }

        function success(title, msg){
            toastr.success(title, msg);
        }

        function error(title, msg){
            toastr.error(title, msg);
        }

        // endregion

        // region Public API

        return {
            info: info,
            success: success,
            error: error
        };

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'messageService',
        type: 'factory',
        service: ['toastr', messageService]
    };

    // endregion

})();
