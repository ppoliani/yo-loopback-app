/**
 * view init (taken form the template)
 */
 (function(){
    'use strict';

    function viewInitDirective(viewInitService){

        function link(){
            viewInitService();
        }

        return {
            restrict: 'AE',
            scope: {},
            link: link
        };
    }

    // region CommonJS

    module.exports = {
        name: 'viewInit',
        directive: [
            'viewInitService',
            viewInitDirective
        ]
    };

    // endregion

 })();