/**
 * Pagination
 */
(function(){
    'use strict';

    function paginationDirective(){

        function paginationCtrl(){

            // region Viewmodel

            this.pageRange = new Array(Math.ceil(this.count / this.pageItems));

            this.changePage = function changePage(page){
                this.currentPage = page;
                this.update();
            };

            // endregion

        }

        return {
            restrict: 'AE',
            templateUrl: '/app/components/pagination/pagination.html',
            scope: {
                currentPage: '=',
                count: '@',
                pageItems: '@',
                update: '&'
            },
            controller: [paginationCtrl],
            controllerAs: 'vm',
            bindToController: true
        };
    }

    // region CommonJS

    module.exports = {
        name: 'pagination',
        directive: [paginationDirective]
    };

    // endregion

})();