var fs          = require('fs'),
    Promise     = require('bluebird'),
    lwip        = require('lwip');

module.exports = function(Image) {

    // region Inner Methods

    /**
     * Reads the image dimension form the request object
     * @param req
     * @private
     */
    function _getImageDims(req){
        var width = Number(req.query.width) || 100,
            height = Number(req.query.height) || 100;

        return {
            width: width,
            height: height
        };
    }

    /**
     * Finds the image with the given id resizes it and streams it into
     * the response stream
     * @param id
     * @param width
     * @param height
     * @param res
     * @private
     */
    function _findResizeImg(id, width, height, res){
        Image.findById(id, function(err, img){
            if(err) res(err);
            else if(img){
                lwip.openAsync(img.data, 'jpg')
                    .then(function(image){
                        Promise.promisifyAll(image);

                        return image.resizeAsync(width, height);
                    })
                    .then(function(rszImg){
                        return rszImg.toBufferAsync('jpg');
                    })
                    .then(function(image){
                        res.end(image, 'application/octet-stream');
                    })
                    .catch(function(err){
                        res('Image not found: ' + err);
                    });
            }
            else{
                res.send(404, 'Image not found');
            }
        });
    }

    // endregion

    // region Remote Methods

    Image.resize = function resize(id, dims, res, clb){
        _findResizeImg(id, dims.width, dims.height, res);
    };

    Image.remoteMethod('resize', {
        accepts: [
            { arg: 'id', type: 'string', http: function(ctx){ return ctx.req.param('id'); } },
            { arg: 'dims', type: 'object', http: function(ctx){ return _getImageDims(ctx.req); } },
            { arg: 'res', type: 'object', http: function(ctx){ return ctx.res; } } // get the res instance as a parameter; Couldn't find a better way
        ],
        returns: { type: 'buffer', root: true },
        http: { path:'/property/:id', verb: 'get' }
    });

    // endregion

    // Promisisfication
    Promise.promisifyAll(lwip);
};