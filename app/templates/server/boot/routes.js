var config = require('../config/');

function registerRoutes(app){
    var router = app.loopback.Router(),
        _devMode = config.get("dev");

    router.get('/', function(req, res) {
        res.render('index.ejs', { dev: true });
    });

    // Support Html5 mode
    router.all('/*', function(req, res, next) {
        var parts = req.url.split('.'),
            fileType = parts[parts.length - 1];

        if(fileType !== 'css'){
            res.render('index.ejs', { dev: true });
        }
        else {
            next();
        }
    });

    app.use(router);
}

module.exports = registerRoutes;