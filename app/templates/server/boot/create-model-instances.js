var debug = require('debug')('boot:create-model-instances');

function createInstanceModels(app){
    var User = app.models.User,
        Role = app.models.Role,
        RoleMapping = app.models.RoleMapping;

    // region Setup

    // create a default admin user
    User.create([{
        username: 'admin',
        email: 'ppoliani@gmail.com',
        password: 'admin'
    }], on_usersCreated);

    // endregion

    // region Events

    function on_usersCreated(err, users){
        if(err) throw err;

        var adminRole = {
            name: 'admin'
        };

        Role.create(adminRole, function on_roleCreate(err, role){
            if(err) throw err;

            debug(role);

            // make default user an admin
            role.principals.create({
                principalType: RoleMapping.USER,
                principalId: users[0].id
            }, on_roleAssigned);
        });
    }

    function on_roleAssigned(err, principal){
        if (err) throw err;
        debug(principal);
    }

    // endregion
}

module.exports = createInstanceModels;