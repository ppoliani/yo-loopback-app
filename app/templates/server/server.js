var loopback = require('loopback'),
    path     = require('path'),
    boot     = require('loopback-boot'),
    bodyParser = require('body-parser');

var app = module.exports = loopback();

// Set up the /favicon.ico
app.use(loopback.favicon());
app.use(loopback.logger('dev'));

// request pre-processing middleware
app.use(loopback.compress());

// -- Add your pre-processing middleware here --
app.use(bodyParser.urlencoded({ extended: true }));

app.use(loopback.static(path.resolve(__dirname, '../client')));

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname);

// -- Mount static files here--
// All static middleware should be registered at the end, as all requests
// passing the static middleware are hitting the file system

app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'ejs');

app.set('json spaces', 2); //pretty print json responses

// Requests that get this far won't be handled
// by any middleware. Convert them into a 404 error
// that will be handled later down the chain.
app.use(loopback.urlNotFound());

// The ultimate error handler.
app.use(loopback.errorHandler());

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    console.log('Web server listening at: %s', app.get('url'));
  });
};

// start the server if `$ node server.js`
if (require.main === module) {
  app.start();
}
