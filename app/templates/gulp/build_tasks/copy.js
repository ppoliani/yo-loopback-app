module.exports = function(gulp, config){
    gulp.task('copy', function(){
        gulp.start('copy:fonts');
    });

    gulp.task('copy:fonts', function () {
        return gulp.src([config.libsDir + '/font-awesome/fonts/**/*.*'])
            .pipe(gulp.dest(config.distDir + '/fonts'));
    });
};